#War Card Game App
from classes import deck,player

#create the deck and shuffle
war_deck = deck.Deck()
war_deck.shuffle_cards()

#create two players and set game state
player1 = player.Player("Player 1")
player2 = player.Player("Player 2")
game_on = True
round_num = 0

#split deck between two players and add the half decks to each players hand
def split_deck(a_list):
    half = len(a_list)//2
    hand1 = a_list[:half]
    hand2 = a_list[half:]
    player1.add_card_to_hand(hand1)
    player2.add_card_to_hand(hand2)
    war_deck.cards = []
   

split_deck(war_deck.cards)

while game_on:
    if len(player1.hand) == 0:
        print('Player 2 wins!')
        game_on = False
        break
    if len(player2.hand) == 0:
        print('Player 1 wins!')
        game_on = False
        break

    round_num = round_num + 1
    print(f'It is round {round_num}')

    #create empty list for cards played each turn
    player_one_cards = []
    player_two_cards = []

    #each player plays a card and they are compared
    player_one_cards.append(player1.remove_card_from_hand())
    print(f'Player 1 plays the ' + str(player_one_cards[-1]))
    player_two_cards.append(player2.remove_card_from_hand())
    print(f'Player 2 plays the ' + str(player_two_cards[-1]))

    at_war = True

    while at_war:

        if player_one_cards[-1].value > player_two_cards[-1].value:
            player1.add_card_to_hand(player_one_cards)
            player1.add_card_to_hand(player_two_cards)
            print('Player 1 wins this round')
            at_war = False
            break
        elif player_two_cards[-1].value > player_one_cards[-1].value:
            player2.add_card_to_hand(player_one_cards)
            player2.add_card_to_hand(player_two_cards)
            print('Player 2 wins this round')
            at_war = False
            break
        else:
            print('WAR!')
            if len(player1.hand) < 5:
                game_on = False
                print('Player 1 unable to complete war due to lack of cards')
                print('Player 2 wins!')
                break
            elif len(player2.hand) < 5:
                game_on = False
                print('Player 2 unable to complete war due to lack of cards')
                print('Player 1 wins!')
                break
            else:
                for num in range(5):
                    player_one_cards.append(player1.remove_card_from_hand())
                    player_two_cards.append(player2.remove_card_from_hand())

                        

